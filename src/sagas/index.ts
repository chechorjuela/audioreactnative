import {all, fork} from 'redux-saga/effects';
import {watchDecreaseCounter} from './homeSaga';

// Imports: Redux Sagas

// Redux Saga: Root Saga
export default function* rootSaga() {
  yield all([fork(watchDecreaseCounter)]);
}
