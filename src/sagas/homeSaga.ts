import {takeLatest, put, call} from 'redux-saga/effects';
import {
  GET_ARTIST_REQUEST,
  GET_ARTIST_ERROR,
  GET_ARTIST_INFO_SUCCESS,
  GET_ARTIST_INFO_REQUEST, GET_ARTIST_INFO_ERROR,
} from '../app/screen/home/constants';
import {getRequest} from '../api/apiAudio';
import * as actions from '../app/screen/home/actions';
import * as actionsDetail  from "../app/screen/home/detail/actions";

type ParamsCreateCourse = {payload: any; type: string};

export function* getArtist(ParamsCreateCourse: any) {
  const {response, error} = yield call(() =>
    getRequest(
      'http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=' +
        ParamsCreateCourse.payload.country +
        '&api_key=829751643419a7128b7ada50de590067&format=json&page=' +
        ParamsCreateCourse.payload.page,
    ),
  );
  try {
    yield put(actions.getArtistSuccess(response));
  } catch (err) {
    yield put({type: GET_ARTIST_ERROR, error});
  }
}
export function* getInfoArtist(ParamsCreateCourse: any) {
  const {response, error} = yield call(() =>
    getRequest(
      'http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=' +
        ParamsCreateCourse.payload +
        '&api_key=829751643419a7128b7ada50de590067&format=json&page=' +
        ParamsCreateCourse.payload.page,
    ),
  );
  try {
    yield put(actionsDetail.getArtistInfoSuccess(response));
  } catch (err) {
    yield put({type: GET_ARTIST_INFO_ERROR, error});
  }
}
// Generator: Watch decrease Counter
export function* watchDecreaseCounter() {
  yield takeLatest(GET_ARTIST_REQUEST, getArtist);
  yield takeLatest(GET_ARTIST_INFO_REQUEST, getInfoArtist);
}
