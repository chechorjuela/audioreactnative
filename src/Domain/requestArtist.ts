export interface RequestArtist {
  method: string;
  country: string;
  api_key: string;
  format: string;
  page: number;
}
