import {
  GET_ARTIST_INFO_REQUEST,
  GET_ARTIST_INFO_SUCCESS,
  GET_ARTIST_REQUEST,
  GET_ARTIST_SUCCESS,
} from '../app/screen/home/constants';
// Initial State
const initialState = {
  artist: [],
  artistInfo: {},
};

// Redux: Counter Reducer
const homeReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_ARTIST_REQUEST: {
      return {
        ...state,
      };
    }
    case GET_ARTIST_SUCCESS: {
      return {
        ...state,
        artist: action.payload,
      };
    }
    case GET_ARTIST_INFO_REQUEST: {
      return {
        ...state,
      };
    }
    case GET_ARTIST_INFO_SUCCESS: {
      return {
        ...state,
        artistInfo: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
// Exports
export default homeReducer;
