import {combineReducers} from 'redux';

// Imports: Reducers
import homeReducer from './homeReducer';

// Redux: Root Reducer
const rootReducer = combineReducers({
  homeR: homeReducer,
});

// Exports
export default rootReducer;
