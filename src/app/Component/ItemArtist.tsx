import React from 'react';
import {Artist} from '../../Domain/artist';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {FadeInImage} from './FadeInImage';

interface Props {
  artist: Artist;
}
const windowWith = Dimensions.get('window').width;

export const ItemArtist = ({artist}: Props) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={{
        marginHorizontal: 2,
        paddingBottom: 15,
        paddingHorizontal: 5,
      }}
      activeOpacity={0.8}
      onPress={() => navigation.navigate('DetailScreen', artist)}>
      <View style={styles.containerCard}>
        <View style={styles.containerImage}>
          <FadeInImage uri={artist.image[0]?.['#text']} style={styles.image} />
        </View>
        <View>
          <Text style={styles.title}>{artist.name}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  containerCard: {
    width: windowWith * 0.4,
    flex: 1,
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    borderRadius: 10,
    shadowOffset: {
      width: 10,
      height: 20,
    },
  },
  containerImage: {
    width: 80,
    height: 80,
    borderRadius: 30,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  image: {
    flex: 2,
    width: 80,
    height: 50,
  },
});
