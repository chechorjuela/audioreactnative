import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeComponent from '../screen/home/HomeComponent';
import DetailComponent from '../screen/home/detail/DetailComponent';
import {Artist} from '../../Domain/artist';
export type RootStackParams = {
  HomeScreen: undefined;
  DetailScreen: Artist;
};
const Stack = createStackNavigator<RootStackParams>();

const RootNavigation = () => {
  return (
    <Stack.Navigator

      initialRouteName="HomeScreen">
      <Stack.Screen name="HomeScreen" component={HomeComponent} />
      <Stack.Screen name="DetailScreen" component={DetailComponent} />
    </Stack.Navigator>
  );
};
export default RootNavigation;
