import {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {Artist} from '../../Domain/artist';

export const useArtist = () => {
  const [isLoading, setLoading] = useState(true);
  const [artists, setArtist] = useState<Artist[]>([]);

  let artistListState = useSelector((state: any) => state.homeR.artist);
  const getArtist = async () => {
    setArtist(artistListState);
    setLoading(false);
  };
  useEffect(() => {
    getArtist();
  }, []);

  return {
    artists,
    isLoading,
  };
};
