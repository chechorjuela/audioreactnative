import React, {useEffect, useRef, useState} from 'react';
import RNPickerSelect from 'react-native-picker-select';
import {Picker} from '@react-native-picker/picker';
import {
  ActivityIndicator,
  FlatList,
  Platform,
  Text,
  View,
} from 'react-native';
import {getArtist} from './actions';
import {useDispatch, useSelector} from 'react-redux';
import {Artist} from '../../../Domain/artist';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {ItemArtist} from '../../Component/ItemArtist';

const HomeComponent = () => {
  const {top} = useSafeAreaInsets();
  const dispatch = useDispatch();
  const artistListState = useSelector((state: any) => state.homeR.artist);
  const [country, setCountry] = useState('colombia');
  const [loading, setLoading] = useState(false);
  const isMounted = useRef(false);
  const [listArtist, setListArtist] = useState<Artist[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [listCountry] = useState([
    {label: 'Colombia', value: 'colombia'},
    {label: 'Spain', value: 'spain'},
    {label: 'USA', value: 'usa'},
  ]);
  useEffect(() => {
    let params: any = {
      country: country,
      page: currentPage,
    };
    dispatch(getArtist(params));

    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    if (typeof artistListState.topartists !== 'undefined') {
      let artistFilter: Artist[] = [];
      artistListState.topartists.artist.map((artist: Artist) => {
        if (listArtist.length > 0) {
          if (!listArtist.find(a => a.mbid === artist.mbid)) {
            artistFilter.push(artist);
          }
        } else {
          artistFilter.push(artist);
        }
      });

      setListArtist([...listArtist, ...artistFilter]);
      setLoading(false);
    }
  }, [artistListState]);
  const loadingArtist = () => {
    let nextPage = currentPage + 1;
    setCurrentPage(nextPage);

    let params: any = {
      country: country,
      page: nextPage,
    };
    dispatch(getArtist(params));
  };
  const onChange = (value: any) => {
    setCountry(value);
    setCurrentPage(1);
    setLoading(true);
    let params: any = {
      country: country,
      page: currentPage,
    };
    setListArtist([]);
    dispatch(getArtist(params));
  };
  return (
    <View
      style={{
        flex: 1,
        marginTop: Platform.OS == 'ios' ? top : top + 10,
        marginHorizontal: 20,
      }}>
      <View
        style={{
          alignContent: 'center',
        }}>
        <Picker
          selectedValue={country}
          onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}>
          <Picker.Item label="Spain" value="spain" />
          <Picker.Item label="Colombia" value="colombia" />
        </Picker>
      </View>
      <FlatList
        data={listArtist}
        keyExtractor={artistList => artistList.mbid}
        showsVerticalScrollIndicator={false}
        numColumns={2}
        ListHeaderComponent={
          <Text
            style={{
              textTransform: 'capitalize',
              fontSize: 24,
              fontWeight: 'bold',
              paddingBottom: 15,
            }}>
            Artist's {country}
          </Text>
        }
        renderItem={({item}) => <ItemArtist artist={item} />}
        onEndReached={loadingArtist}
        onEndReachedThreshold={0.5}
        ListFooterComponent={<ActivityIndicator color={'red'} />}
      />
    </View>
  );
};

export default HomeComponent;
