import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackParams} from '../../../navigation/rootNavigation';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {getArtistInfo} from './actions';
import {useDispatch, useSelector} from 'react-redux';
import {WelcomeArtist} from '../../../../Domain/ArtistDetail';

interface Props extends StackScreenProps<RootStackParams, 'DetailScreen'> {}

const DetailComponent = ({route}: Props) => {
  const {top} = useSafeAreaInsets();
  const dispatch = useDispatch();
  //const movie = route.params as Artist;
  const [artist, setArtist] = useState<WelcomeArtist>();
  const artisState = useSelector((state: any) => state.homeR.artistInfo);
  useEffect(() => {
    dispatch(getArtistInfo(route.params.name));
  }, [route]);
  useEffect(() => {
    setArtist(artisState.artist);
  }, [artisState, artist]);
  const renderTags = () => {
    return artist?.tags.tag.map((item, index) => (
      <Text style={{fontSize: 12, paddingLeft: 15}} key={index}>
        {item.name}
      </Text>
    ));
  };
  return (
    <View
      style={{
        flex: 1,
      }}>
      {typeof artist !== 'undefined' && (
        <View>
          <View
            style={{
              height: 210,
              zIndex: 999,
              backgroundColor: 'red',
              alignItems: 'flex-start',
              borderBottomEndRadius: 1000,
              borderBottomLeftRadius: 0,
              overflow: 'hidden',
            }}>
            <Text
              style={{
                fontSize: 20,
                margin: 10,
                color: 'white',
                top: top,
                alignItems: 'flex-start',
                textAlign: 'left',
              }}>
              {artist.name}!
            </Text>
            <Text
              style={{
                position: 'absolute',
                right: 0,
                top: 4,
                paddingRight: 10,
                fontWeight: 'bold',
              }}>
              {artist.bio.published}
            </Text>
            <Text
              style={{
                width: 340,
                fontSize: 12,
                margin: 10,
                color: 'white',
                top: top - 20,
                alignItems: 'flex-start',
                textAlign: 'left',
              }}>
              {artist.bio.summary}!
            </Text>
          </View>

          <ScrollView style={{}}>
            <View
              style={{
                height: 180,
              }}>
             
            </View>
            <View style={styles.container}>
              <Text>#TAGS</Text>
              <ScrollView horizontal={true}>{renderTags()}</ScrollView>
              <View>
                <Text style={{fontSize: 18, fontWeight: 'bold'}}>Bio</Text>
              </View>
              <Text>{artist.bio.content}</Text>
            </View>
          </ScrollView>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
  },
  welcome: {},
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default DetailComponent;
