import {GET_ARTIST_INFO_REQUEST, GET_ARTIST_INFO_SUCCESS} from '../constants';

export const getArtistInfo = (payload: any) => ({
  type: GET_ARTIST_INFO_REQUEST,
  payload,
});

export const getArtistInfoSuccess = (payload: any) => ({
  type: GET_ARTIST_INFO_SUCCESS,
  payload,
});
