export const GET_ARTIST_REQUEST = 'GET_ARTIST_REQUEST';
export const GET_ARTIST_SUCCESS = 'GET_ARTIST_SUCCESS';
export const GET_ARTIST_ERROR = 'GET_ARTIST_ERROR';

export const GET_ARTIST_INFO_REQUEST = 'GET_ARTIST_INFO_REQUEST';
export const GET_ARTIST_INFO_SUCCESS = 'GET_ARTIST_INFO_SUCCESS';
export const GET_ARTIST_INFO_ERROR = 'GET_ARTIST_INFO_ERROR';
