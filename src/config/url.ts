import {settings} from './settings';

const artistBase = `${settings.local.urlBase}/`;

export const urlArtist = {
  listArtist: `${artistBase}/`,
};
