const handleRequest = async (request: any) => {
  try {
    const response = await request;
    const json = await response.json();
    const {status} = response;
    if (status === 401) {
    }
    if (status === 403) {
    }
    if (response.ok) {
      return {status, response: json};
    }
    const exception = {status, error: json};
    throw exception;
  } catch (error) {
    return error;
  }
};

export const getRequest = (url: string) => {
  const request = fetch(`${url}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return handleRequest(request);
};
export const deleteRequest = (url: string, id: string) => {
  const request = fetch(`${url}/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return handleRequest(request);
};
export const postRequest = (url: string, obj: any) => {
  const request = fetch(`${url}`, {
    method: 'POST',
    body: JSON.stringify(obj),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return handleRequest(request);
};
export const putRequest = (url: string, id: string, obj: any) => {
  const request = fetch(`${url}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(obj),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return handleRequest(request);
};
