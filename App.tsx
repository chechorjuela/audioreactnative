import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RootNavigation from './src/app/navigation/rootNavigation';
import {Provider} from 'react-redux';
import Store from './src/store/store';

const store = Store();
const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootNavigation/>
      </NavigationContainer>
    </Provider>
   
  );
};

export default App;
